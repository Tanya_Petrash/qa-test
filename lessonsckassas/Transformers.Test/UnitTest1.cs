using System;
using Xunit;
using HW6._1;




namespace HW6.Test
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Transformers transformer = new Transformers();
            string actual = transformer.FlyTransforming();

            Assert.Equal("Fly", actual);

        }
        [Fact]
        public void Test2()
        {
            Transformers transformer = new Transformers();
            string actual = transformer.RideTransforming();

            Assert.Equal("Ride", actual);
        }
        [Fact]
        public void Test3()
        {
            Transformers transformer = new Transformers();
            string actual = transformer.Run();

            Assert.Equal("Run", actual);
        }
        [Fact]
        public void Test4()
        {
            Transformers transformer = new Transformers();
            string actual = transformer.Shoot();

            Assert.Equal("Pifpafff", actual);
        }
        [Fact]
        public void Test5()
        {
            Transformers transformer = new Transformers();
            string actual = transformer.SwimTransforming();

            Assert.Equal("Swim", actual);
        }
       
    }
}
