﻿using System;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace HW6_Library
{
    public class Library
    {
        public List<Book> listBook = new List<Book>() { };
        public void AddBook(string NameBook, string NameAutor, int Year, int CountDays, string Status, DateTime TimeToRead, DateTime DaysLeft)
        {
            Book book = new Book(NameBook, NameAutor, Year, CountDays, Status, TimeToRead, DaysLeft);
            listBook.Add(book);
        }
        public void AddBook(string NameBook, string NameAutor, int Year, int CountDays)
        {
            Book book = new Book(NameBook, NameAutor, Year, CountDays);
            listBook.Add(book);
        }
        public void RemoveBook(List<Book> listBook, string nameBook)//оставляет ячейку пустой, но удаляет корректно
        {
            for (int i = 0; i < listBook.Count; i++)
            {
                if (nameBook.Equals(listBook[i].NameBook))
                {
                    listBook.RemoveAt(i);
                }
            }
        }
        public void LibraryOutputAlphabetically(List<Book> listBook)
        {
            listBook.Sort(delegate (Book x, Book y)
            {
                if (x.NameBook == null && y.NameBook == null) return 0;
                else if (x.NameBook == null) return -1;
                else if (y.NameBook == null) return 1;
                else return x.NameBook.CompareTo(y.NameBook);
            });
        }
        public void FindBookInTheLibrary(string nameBookForSearch, List<Book> listBook)
        {
            for (int i = 0; i < listBook.Count; i++)
            {
                if (listBook[i].NameBook == nameBookForSearch)
                {
                    Console.WriteLine(i + ". " + "'" + listBook[i].NameBook + "'" + " " + listBook[i].NameAutor + " " + listBook[i].Year);
                    Console.WriteLine("\n----------------------------------------------------------");

                }
            }
        }
        public void TakeToRead()
        {
            for (int i = 0; i < listBook.Count; i++)
            {
                Console.WriteLine(i + ". " + "'" + listBook[i].NameBook + "'" + " " + listBook[i].NameAutor + " " + listBook[i].Year + " " + "\nКол-во дней на прочтение: " + listBook[i].CountDays + " \nСтатус: " + listBook[i].Status);
                Console.WriteLine("\n----------------------------------------------------------");

            }

        }

        public void FindAuthorBooks(string nameAutor, List<Book> listBook)
        {
            for (int i = 0; i < listBook.Count; i++)
            {
                if (listBook[i].NameAutor == nameAutor)
                {
                    Console.WriteLine(i + ". " + "'" + listBook[i].NameBook + "'" + " " + listBook[i].NameAutor + " " + listBook[i].Year);
                    Console.WriteLine("\n----------------------------------------------------------");

                }
            }
        }
        public void PrintBooks()
        {
            Console.WriteLine("БИБЛИОТЕКА");
            for (int i = 0; i < listBook.Count; i++)
            {
                Console.WriteLine(i + ". " + "'" + listBook[i].NameBook + "'" + " " + listBook[i].NameAutor + " " + listBook[i].Year);

                Console.WriteLine("\n----------------------------------------------------------");

            }
        }
    }
}
