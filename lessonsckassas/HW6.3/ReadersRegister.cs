﻿using System;
using System.Collections.Generic;
using System.Text;
namespace HW6_Library
{
    class ReadersRegister
    {
        //    public int TimeToRead;
        public List<Readers> listReadersRegister = new List<Readers>() { };
        public DateTime Now = DateTime.Now;

        public bool Registration(string Name, List<Readers> listReadersRegister)
        {
            Readers readers = new Readers();
            bool status = false;
            for (int i = 0; i <= listReadersRegister.Count; i++)
            {
                if (listReadersRegister.Count != 0)
                {
                    if (listReadersRegister[i].Name.Equals(Name) || listReadersRegister[i].Name.Equals("Name"))
                    {
                        status = false;
                        Console.WriteLine("Такой пользователь уже есть\n");//
                        break;
                    }
                    else
                    {
                        status = true;
                        readers.Name = Name;
                        listReadersRegister.Add(readers);
                        break;
                    }
                }
                else
                {
                    status = true;
                    readers.Name = Name;
                    listReadersRegister.Add(readers);
                    break;
                }
            }

            return status;
        }
        public string Autorization(string Name, List<Readers> listReadersRegister, ReadersRegister readersRegister, Library OURlibrary)
        {
            string reader = "";
            if (listReadersRegister.Count != 0)
            {
                for (int i = 0; i < listReadersRegister.Count; i++)
                {
                    if (listReadersRegister[i].Name.Equals(Name))
                    {
                        Console.WriteLine("Вы вошли в свой аккаунт!\n");
                        reader = listReadersRegister[i].Name;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Такого пользователя не существует");
                        Console.WriteLine("Нажмите Enter");
                        Console.ReadKey();
                        Console.Clear();
                        reader = Program.RegistrationMenu(readersRegister, OURlibrary);
                        break;
                    }
                }
            }
            else
            {
                Console.WriteLine("Такого пользователя не существует");
                reader = Program.RegistrationMenu(readersRegister, OURlibrary);
            }

            return reader;
        }


        public void TakeToRead(string nameBook, string nameReader, ReadersRegister readersRegister, Library OURlibrary)
        {
            bool go = false;
            int numBook = 0;
            for (int i = 0; i < OURlibrary.listBook.Count; i++)
            {
                if (OURlibrary.listBook[i].NameBook.Equals(nameBook))
                {
                    if (OURlibrary.listBook[i].Status.Equals("можно взять почитать"))
                    {
                        go = true;
                        numBook = i;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine($"Нельзя взять почитать '{nameBook}'");
                        Console.WriteLine("Нажмите Enter");
                        Console.ReadKey();
                        Console.Clear();
                        Program.TakeBookMenu(nameReader, readersRegister, OURlibrary);
                    }
                }
            }

            for (int i = 0; i < listReadersRegister.Count; i++)
            {
                if (go)
                {
                    if (listReadersRegister[i].Name.Equals(nameReader))
                    {
                        //??????
                        listReadersRegister[i].MyBook.Add(OURlibrary.listBook[numBook]);
                        OURlibrary.listBook[numBook].Status = "читают";
                        for (int j = 0; j < listReadersRegister[i].MyBook.Count; j++)
                        {
                            if (listReadersRegister[i].MyBook[j].NameBook.Equals(OURlibrary.listBook[numBook].NameBook))
                            {
                                listReadersRegister[i].MyBook[j].Status = "читают";
                            }
                        }

                        OURlibrary.listBook[numBook].TimeToRead = Now;
                        Console.WriteLine("Начали читать: " + OURlibrary.listBook[numBook].TimeToRead);

                        OURlibrary.listBook[numBook].DaysLeft = OURlibrary.listBook[numBook].TimeToRead.AddDays(OURlibrary.listBook[numBook].CountDays);
                        Console.WriteLine("Должны вернуть: " + OURlibrary.listBook[numBook].DaysLeft);

                        var res = rentAnotherBook();
                        if (res)
                        {
                            nameBook = Program.GiveToReadMenu(OURlibrary, readersRegister);
                            TakeToRead(nameBook, nameReader, readersRegister, OURlibrary);
                        }

                        Console.WriteLine("Нажмите Enter");
                        Console.ReadKey();
                        Console.Clear();
                        Program.MainMenu(OURlibrary, readersRegister);
                        break;
                        //readersRegister.TimeToRead = int.Parse(startReadTime.Day) + OURlibrary.listBook[i].CountDays;
                    }
                }
                else
                {
                    Console.Clear();
                    Program.TakeBookMenu(nameReader, readersRegister, OURlibrary);
                }
            }
            //Console.WriteLine("Success");
        }

        public bool rentAnotherBook()
        {
            bool result = false;
            Console.WriteLine("Хотите еще взять почитать книгу? (да/нет)");
            Console.Write(" --> ");
            var answer = Console.ReadLine();
            if (answer.Equals("да")) result = true;

            return result;
        }

        public void ReturnBook(string nameBook, string nameReader, ReadersRegister readersRegister, Library OURlibrary)
        {
            var readerBook = "";
            var book = nameBook;
            int temp = 0;
            for (int i = 0; i < readersRegister.listReadersRegister.Count; i++)
            {
                if (listReadersRegister[i].Name.Equals(nameReader))
                {
                    for (int j = 0; j < listReadersRegister[i].MyBook.Count; j++)
                    {
                        if (listReadersRegister[i].MyBook[j].NameBook.Equals(book))
                        {
                            readerBook = listReadersRegister[i].MyBook[j].NameBook;
                            temp = j;
                        }
                    }

                    if (readerBook.Equals(book))
                    {
                        listReadersRegister[i].MyBook[temp].Status = "можно взять почитать";
                        for (int j = 0; j < OURlibrary.listBook.Count; j++)
                        {
                            if (OURlibrary.listBook[j].NameBook.Equals(book))
                            {
                                OURlibrary.listBook[j].Status = "можно взять почитать";
                            }
                        }
                        //MyBook[temp].NameBook.Remove(listReadersRegister[i].MyBook[temp]);

                        Console.WriteLine("Вы вернули Книгу\n" +
                            "Нажмите Enter");
                        Console.ReadKey();
                        Console.Clear();
                        Program.MainMenu(OURlibrary, readersRegister);
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Эту книгу брали почитать не вы!!!\n" +
                            "Нажмите Enter");
                        Console.ReadKey();
                        Console.Clear();
                        Program.MainMenu(OURlibrary, readersRegister);
                        break;
                    }
                }
            }

        }
        // DateTime dat = DateTime.ParseExact(Date, "d'.'M'.'yyyy", new CultureInfo("ru-RU"));
        //AddDays(double value): добавляет к текущей дате несколько дней
    }
}