﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW6_Library
{
    class Readers
    {
        public string Name;
        public List<Book> MyBook = new List<Book>() { };
        public Readers() { }
        public Readers(string Name, List<Book> MyBook)
        {
            this.Name = Name;
            this.MyBook = MyBook;
        }
    }
}
