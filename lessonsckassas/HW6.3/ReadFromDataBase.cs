﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
namespace HW6_Library
{
    public class ReadFromDataBase
    {
        public const string DataBaseBookTxt = @"D:\Таня программы\sourcerepos\lessonsckassas\HW6.3\DataBaseBooks.txt";

        public static List<Book> BDbooks = new List<Book>() { };
        public void ReadFromBD()
        {
            using (FileStream fstream = File.OpenRead($"{DataBaseBookTxt}"))
            {
                // преобразуем строку в байты
                byte[] array = new byte[fstream.Length];
                // считываем данные
                fstream.Read(array, 0, array.Length);
                // декодируем байты в строку
                string textFromFile = System.Text.Encoding.Default.GetString(array);
                string[] stringDB = textFromFile.Split("%");

                for (int i = 0; i < stringDB.Length - 1; i += 7)
                {

                    Book book = new Book(stringDB[i].Trim(), stringDB[i + 1], int.Parse(stringDB[i + 2]), int.Parse(stringDB[i + 3]), stringDB[i + 4], DateTime.Parse(stringDB[i + 5]), Convert.ToDateTime(stringDB[i + 6]));
                    BDbooks.Add(book);
                }
            }
        }
        public static void PrintBDBooks()
        {
            Console.WriteLine("БИБЛИОТЕКА");
            for (int i = 0; i < BDbooks.Count; i++)
            {
                Console.Write(i + ". " + "'" + BDbooks[i].NameBook + "'" + " " + BDbooks[i].NameAutor + " " + BDbooks[i].Year);

                Console.WriteLine("\n----------------------------------------------------------");

            }
        }

    }
}
