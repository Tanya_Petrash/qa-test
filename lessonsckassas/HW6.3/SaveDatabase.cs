﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HW6_Library
{
    public class SaveDatabase
    {
        public string DataBaseBookTxt = @"D:\Таня программы\sourcerepos\lessonsckassas\HW6.3\DataBaseBooks.txt";
        public string SaveBooksTxt(Library OURlibrary)  // в скобках параметр который мы принимаем
        {
            try
            { //Encoding содержит кодировку выходных данных
                using (StreamWriter res = new StreamWriter(DataBaseBookTxt, false, System.Text.Encoding.Default)) // создаем объект
                {
                    for (int i = 0; i < OURlibrary.listBook.Count; i++)
                    {
                        string text = (OURlibrary.listBook[i].NameBook + "%" + OURlibrary.listBook[i].NameAutor + "%" + OURlibrary.listBook[i].Year + "%" + OURlibrary.listBook[i].CountDays + "%" + OURlibrary.listBook[i].Status + "%" + OURlibrary.listBook[i].TimeToRead + "%" + OURlibrary.listBook[i].DaysLeft + "%").ToString();
                        res.WriteLine(text);  // сама запись в файл
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return DataBaseBookTxt;
        }

    }
}
