﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace HW6_Library
{
    class Program
    {
        public const string DataBaseBookTxt = @"D:\Таня программы\sourcerepos\lessonsckassas\HW6.3\DataBaseBooks.txt";

        static void Main(string[] args)
        {
            ReadersRegister readersRegister = new ReadersRegister();
            Library OURlibrary = new Library();

            UpdateReadersDB(readersRegister);

            UpdateBookDB(OURlibrary);

            //var res = ValidationNumber(readersRegister, OURlibrary);
            //Console.WriteLine(res);
            //ExeptionHanding();
            MainMenu(OURlibrary, readersRegister);
        }
        public static void UpdateReadersDB(ReadersRegister readersRegister)
        {
            ReadFromDBReaders readers = new ReadFromDBReaders();
            var listOldReader = readers.ReadFromBD();
            if (listOldReader.Count != 0)
            {
                readersRegister.listReadersRegister = listOldReader;
            }
        }
        public static void UpdateBookDB(Library OURlibrary)
        {
            ReadFromDataBase readFromDataBase = new ReadFromDataBase();
            readFromDataBase.ReadFromBD();
            var BookFromBd = ReadFromDataBase.BDbooks;
            if (BookFromBd.Count == 0)
            {
                OURlibrary.AddBook("Хроники Нарнии", "Клайв Льюис", 1987, 30);
                OURlibrary.AddBook("Java Полное руководство", "Герберт Шилдт", 1283, 50);
                OURlibrary.AddBook("Скотный двор", "Джордж Оруэлл", 1434, 4);
                OURlibrary.AddBook("Alice in the Wonderland", "Lewis Carroll", 2012, 5);
                OURlibrary.AddBook("Боль", "Клайв Льюис", 1987, 14);
            }
            else
            {
                for (int i = 0; i < BookFromBd.Count; i++)
                {
                    OURlibrary.AddBook(BookFromBd[i].NameBook, BookFromBd[i].NameAutor, BookFromBd[i].Year, BookFromBd[i].CountDays, BookFromBd[i].Status, BookFromBd[i].TimeToRead, BookFromBd[i].DaysLeft);
                }
                for (int i = 0; i < BookFromBd.Count; i++)
                {
                    if (OURlibrary.listBook[i].Status.Equals("читают"))
                    {
                        var yearDedline = OURlibrary.listBook[i].DaysLeft.Year;
                        var monthDedline = OURlibrary.listBook[i].DaysLeft.Month;
                        var dayDedline = OURlibrary.listBook[i].DaysLeft.Day;

                        DateTime Now = DateTime.Now;
                        var yearNow = Now.Year;
                        var monthNow = Now.Month;
                        var dayNow = Now.Day;

                        DateTime deadline = new System.DateTime(yearDedline, monthDedline, dayDedline);
                        DateTime NowTime = new System.DateTime(yearNow, monthNow, dayNow);

                        System.TimeSpan diffDate = deadline - NowTime;
                        if (diffDate.Days <= 0)
                        {
                            OURlibrary.listBook[i].Status = "можно взять почитать";
                        }
                    }
                }

            }
        }
        public static void MainMenu(Library OURlibrary, ReadersRegister readersRegister)
        {
            Console.WriteLine("ГЛАВНОЕ МЕНЮ");
            Console.WriteLine("Добавить книгу в каталог -> 1\n" +
                "Исключить книгу из каталога -> 2\n" +
                "Вывести содержимое каталога по алфавиту -> 3\n" +
                "Вывести список книг определенного автора -> 4\n" +
                "Найти книгу в списке -> 5\n" +
                "Взять почитать -> 6\n" +
                "Вернуть книгу -> 7\n" +
                "Сохранить и выйти -> 8");
            var str = ValidationEnter(readersRegister, OURlibrary);
            var temp = int.Parse(str);
            switch (temp)
            {
                case 1:
                    AddFromAddMenu(OURlibrary, readersRegister);
                    OURlibrary.PrintBooks();
                    Console.WriteLine();
                    Console.WriteLine("\nНажми Enter");
                    Console.ReadKey();
                    Console.Clear();
                    MainMenu(OURlibrary, readersRegister);
                    break;
                case 2:
                    Console.WriteLine("\n СПИСОК ВСЕХ КНИГ \n");
                    OURlibrary.PrintBooks();
                    var nameRemove = menuRemove(OURlibrary, readersRegister);
                    Console.WriteLine();
                    OURlibrary.RemoveBook(OURlibrary.listBook, nameRemove);
                    Console.Clear();
                    OURlibrary.PrintBooks();
                    Console.WriteLine("\nНажми Enter");
                    Console.ReadKey();
                    Console.Clear();
                    MainMenu(OURlibrary, readersRegister);
                    break;
                case 3:
                    OURlibrary.LibraryOutputAlphabetically(OURlibrary.listBook);
                    OURlibrary.PrintBooks();
                    Console.WriteLine("\nНажми Enter");
                    Console.ReadKey();
                    Console.Clear();
                    MainMenu(OURlibrary, readersRegister);
                    break;
                case 4:
                    OURlibrary.PrintBooks();
                    var nameAutor = SearchOfAutorMenu(OURlibrary, readersRegister);
                    Console.Clear();
                    OURlibrary.FindAuthorBooks(nameAutor, OURlibrary.listBook);
                    Console.WriteLine("\nНажми Enter");
                    Console.ReadKey();
                    Console.Clear();
                    MainMenu(OURlibrary, readersRegister);
                    break;
                case 5:
                    var nameBookForSearch = SearchOfNameBookMenu(OURlibrary, readersRegister);
                    Console.Clear();
                    OURlibrary.FindBookInTheLibrary(nameBookForSearch, OURlibrary.listBook);
                    Console.WriteLine("\nНажми Enter");
                    Console.ReadKey();
                    Console.Clear();
                    MainMenu(OURlibrary, readersRegister);
                    break;
                case 6:
                    AccountMenu(readersRegister, OURlibrary);
                    break;
                case 7:
                    Console.Clear();
                    //ReturnBookMenu(OURlibrary);
                    ReturnBookMenu(readersRegister, OURlibrary);
                    break;
                case 8:
                    SaveDatabase saveDatabase = new SaveDatabase();
                    saveDatabase.SaveBooksTxt(OURlibrary);
                    SaveDBReaders readers = new SaveDBReaders();
                    readers.SaveReadersTxt(readersRegister);
                    Console.WriteLine("Bye");
                    break;
            }
        }
        static void AddFromAddMenu(Library OURlibrary, ReadersRegister readersRegister)
        {
            Console.WriteLine("Заполните поля книги, которую хотите добавить\n");
            Console.Write("НАЗВАНИЕ КНИГИ ");
            var nameBook = ValidationEnter(readersRegister, OURlibrary);
            //!Book
            Console.Write("АВТОР ");
            var autor = ValidationEnter(readersRegister, OURlibrary);
            Console.Write("ГОД ВЫПУСКА ");
            var str = ValidationEnter(readersRegister, OURlibrary);
            int year = int.Parse(str);
            Console.Write("НА СКОЛЬКО ДНЕЙ МОЖНО ВЗЯТЬ КНИГУ ");
            str = ValidationEnter(readersRegister, OURlibrary);
            int countDays = int.Parse(str);
            Console.WriteLine();
            OURlibrary.AddBook(nameBook, autor, year, countDays);
        }
        static string menuRemove(Library OURlibrary, ReadersRegister readersRegister)
        {
            string nameRemove = "";
            Console.Write("Введите номер книги, которую хотите удалить --> ");
            var str = ValidationEnter(readersRegister, OURlibrary);
            var num = int.Parse(str);
            for (int i = 0; i < OURlibrary.listBook.Count; i++)
            {
                if (i == num)
                {
                    nameRemove = OURlibrary.listBook[i].NameBook;
                }
            }
            return nameRemove;
        }
        public static string GiveToReadMenu(Library OURlibrary, ReadersRegister readersRegister)
        {
            string nameBook = "";
            Console.Write("\nВведите номер книги, которую хотите почитать: ");

            var str = ValidationEnter(readersRegister, OURlibrary);
            var num = int.Parse(str);
            for (int i = 0; i < OURlibrary.listBook.Count; i++)
            {
                if (i == num)
                {
                    nameBook = OURlibrary.listBook[i].NameBook;
                }
            }
            return nameBook;
        }
        static string ReturnBookMenu(Library OURlibrary, ReadersRegister readersRegister)
        {
            OURlibrary.TakeToRead();
            string nameBook = "";
            Console.Write("\nВведите номер книги, которую хотите вернуть: ");

            var str = ValidationEnter(readersRegister, OURlibrary);
            var num = int.Parse(str);
            for (int i = 0; i < OURlibrary.listBook.Count; i++)
            {
                if (i == num)
                {
                    nameBook = OURlibrary.listBook[i].NameBook;
                }
            }
            return nameBook;
        }
        static string SearchOfAutorMenu(Library OURlibrary, ReadersRegister readersRegister)
        {
            string resNameAutor = "";
            Console.Write("\nВведите имя автора, книги которого вы хотите найти: ");
            var nameAutor = ValidationEnter(readersRegister, OURlibrary);
            Console.WriteLine();
            for (int i = 0; i < OURlibrary.listBook.Count; i++)
            {
                if (OURlibrary.listBook[i].NameAutor.Contains(nameAutor))
                {
                    resNameAutor = OURlibrary.listBook[i].NameAutor;
                }
            }
            return resNameAutor;
        }
        static string SearchOfNameBookMenu(Library OURlibrary, ReadersRegister readersRegister)
        {
            string resNameBook = "";
            Console.Write("\nВведите название книги, чтобы найти ее в библиотеке: ");
            var nameBook = ValidationEnter(readersRegister, OURlibrary);
            Console.WriteLine();
            for (int i = 0; i < OURlibrary.listBook.Count; i++)
            {
                if (OURlibrary.listBook[i].NameBook.Contains(nameBook))
                {
                    resNameBook = OURlibrary.listBook[i].NameBook;
                }
                else
                {
                    Console.WriteLine("Ничего не найденно...\n");

                    Console.WriteLine("Нажмите Enter");
                    Console.ReadKey();
                    Console.Clear();
                    MainMenu(OURlibrary, readersRegister);
                }
            }
            return resNameBook;
        }
        static void AccountMenu(ReadersRegister readersRegister, Library OURlibrary)
        {
            Console.Clear();
            Console.WriteLine("ВОЙДИТЕ В АККАУНТ: \n" +
                "1. Зарегистрироваться  --> 1\n" +
                "2. Войти в существующий аккаунт --> 2\n" +
                "3. Вернуться в главное меню --> 3");
            var str = ValidationEnter(readersRegister, OURlibrary);
            int num = int.Parse(str);
            switch (num)
            {
                case 1:
                    var newName = RegistrationMenu(readersRegister, OURlibrary);
                    TakeBookMenu(newName, readersRegister, OURlibrary);
                    break;
                case 2:
                    var readerName = AutorizationMenu(readersRegister, OURlibrary);
                    TakeBookMenu(readerName, readersRegister, OURlibrary);
                    break;
                case 3:
                    MainMenu(OURlibrary, readersRegister);
                    break;
            }

        }
        public static string RegistrationMenu(ReadersRegister readersRegister, Library OURlibrary)
        {
            Console.WriteLine("Хотите зарегестрироваться?\n" +
                "ДА --> 1\nНЕТ --> 2");
            //?????????????????

            var str = ValidationEnter(readersRegister, OURlibrary);
            int num = int.Parse(str);
            if (num == 2)
                AccountMenu(readersRegister, OURlibrary);

            Console.Write("Введите имя вашего аккаунта: ");
            var name = ValidationEnter(readersRegister, OURlibrary);
            var registration = readersRegister.Registration(name, readersRegister.listReadersRegister);
            if (registration)
            {
                Console.WriteLine("\nУСПЕШНАЯ РЕГИСТРАЦИЯ! ");
                Console.WriteLine("Нажмите Enter, чтобы выбрать книгу");
                Console.ReadKey();
                Console.Clear();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Попробуй еще раз...");
                name = "error";  //?
                AccountMenu(readersRegister, OURlibrary);
            }
            var count = readersRegister.listReadersRegister.Count; // если нужно будет имя того, кто зарегестрировался 
            for (int i = count; i > 0; i--)
            {
                name = readersRegister.listReadersRegister[count - 1].Name;
            }
            return name;
        }
        public static string AutorizationMenu(ReadersRegister readersRegister, Library OURlibrary)
        {
            Console.Write("Введите имя вашего аккаунта: ");
            var name = ValidationEnter(readersRegister, OURlibrary);

            return readersRegister.Autorization(name, readersRegister.listReadersRegister, readersRegister, OURlibrary);
        }
        public static void TakeBookMenu(string nameReader, ReadersRegister readersRegister, Library OURlibrary)
        {
            OURlibrary.TakeToRead();
            var nameBook = GiveToReadMenu(OURlibrary, readersRegister);
            readersRegister.TakeToRead(nameBook, nameReader, readersRegister, OURlibrary);

        }
        public static void ReturnBookMenu(ReadersRegister readersRegister, Library OURlibrary)
        {
            Console.Clear();
            OURlibrary.TakeToRead();
            Console.WriteLine("Выберите книгу, которую хотите вернуть");
            var str = ValidationEnter(readersRegister, OURlibrary);
            int book = int.Parse(str);

            if (OURlibrary.listBook[book].Status.Equals("читают"))
            {
                Console.WriteLine("Введите ваше имя");
                str = ValidationEnter(readersRegister, OURlibrary);
                var nameReader = str;
                readersRegister.ReturnBook(OURlibrary.listBook[book].NameBook, nameReader, readersRegister, OURlibrary);
            }
            else
            {
                Console.WriteLine("Эту книгу никто не брал почитать\n" +
                    "Нажмите Enter");
                Console.ReadKey();
                Console.Clear();
                MainMenu(OURlibrary, readersRegister);

            }
            //string nameReader

        }

        public static string ValidationEnter(ReadersRegister readersRegister, Library OURlibrary)
        {
            Console.Write(" --> ");
            string str = Console.ReadLine();
            if (string.IsNullOrEmpty(str))
            {
                Console.WriteLine("НЕКОРРЕКТНЫЙ ВВОД\n" +
                    "Нажмите Enter");
                Console.ReadKey();
                Console.Clear();
                MainMenu(OURlibrary, readersRegister);
                return "";
            }
            return str;
        }

    }
}
