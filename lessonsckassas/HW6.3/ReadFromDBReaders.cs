﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HW6_Library
{
    class ReadFromDBReaders
    {
        public const string DataBaseReadersTxt = @"D:\Таня программы\sourcerepos\lessonsckassas\HW6.3\DataBaseReaders.txt";

        public List<Readers> ReadFromBD()
        {
            ReadersRegister readersRegister = new ReadersRegister();
            var listReaders = readersRegister.listReadersRegister;

            using (FileStream fstream = File.OpenRead($"{DataBaseReadersTxt}"))
            {
                // преобразуем строку в байты
                byte[] array = new byte[fstream.Length];
                // считываем данные
                fstream.Read(array, 0, array.Length);
                // декодируем байты в строку
                string textFromFile = System.Text.Encoding.Default.GetString(array);
                string[] stringDBReaders = textFromFile.Split("Name");

                //stringDBReaders[i].Equals(stringDBReaders[i + 2].Replace("\r\n", ""))
                for (int i = 1; i < stringDBReaders.Length; i++)
                {
                    Readers readers = new Readers();
                    for (int j = 1; j < stringDBReaders.Length; j += stringDBReaders.Length)
                    {
                        var NameAndBook = stringDBReaders[i].Split("Book");
                        for (int k = 1; k < NameAndBook.Length; k++)
                        {
                            var NameBooks = NameAndBook[k].Split("*");
                            for (int v = 0; v < NameBooks.Length; v++)
                            {
                                Book book = new Book(NameBooks[v].Replace("\r\n", ""));
                                readers.MyBook.Add(book);
                            }

                        }
                        readers = new Readers(NameAndBook[0].Trim(), readers.MyBook);

                    }
                    listReaders.Add(readers);
                }
            }
            //PrintBDReaders(readersRegister);
            return listReaders;
        }
        public static void PrintBDReaders(ReadersRegister readersRegister)
        {
            Console.WriteLine("БИБЛИОТЕКА");
            for (int i = 0; i < readersRegister.listReadersRegister.Count; i++)
            {
                for (int j = 0; j < readersRegister.listReadersRegister[i].MyBook.Count; j++)
                {
                    Console.Write(i + ". " + readersRegister.listReadersRegister[i].Name + " " + readersRegister.listReadersRegister[i].MyBook[j].NameBook);

                }
                Console.WriteLine("\n----------------------------------------------------------");

            }
        }
    }
}




