﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
namespace HW6_Library
{
    class SaveDBReaders
    {
        public string DataBaseBookTxt = @"D:\Таня программы\sourcerepos\lessonsckassas\HW6.3\DataBaseReaders.txt";

        public void SaveReadersTxt(ReadersRegister readersRegister)  // в скобках параметр который мы принимаем
        {
            try
            { //Encoding содержит кодировку выходных данных
                using (StreamWriter res = new StreamWriter(DataBaseBookTxt, false, System.Text.Encoding.Default)) // создаем объект
                {
                    string text;
                    for (int i = 0; i < readersRegister.listReadersRegister.Count; i++)
                    {
                        text = ("Name" + readersRegister.listReadersRegister[i].Name + "Book");
                        for (int j = 0; j < readersRegister.listReadersRegister[i].MyBook.Count; j++)
                        {
                            if (j == readersRegister.listReadersRegister[i].MyBook.Count - 1 || readersRegister.listReadersRegister[i].MyBook.Count == 1)
                                text += (readersRegister.listReadersRegister[i].MyBook[j].NameBook).ToString();
                            else text += (readersRegister.listReadersRegister[i].MyBook[j].NameBook + "*").ToString();

                        }

                        res.WriteLine(text);

                    }
                    // сама запись в файл
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
