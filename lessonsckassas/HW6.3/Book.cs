﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW6_Library
{
    public class Book
    {
        public string NameBook;
        public string NameAutor;
        public int Year;
        public int CountDays;
        public string Status = "можно взять почитать";
        public DateTime TimeToRead;
        public DateTime DaysLeft;

        public Book(string NameBook)
        {
            this.NameBook = NameBook;
        }
        public Book(string NameBook, string NameAutor, int Year, int CountDays)
        {
            this.NameBook = NameBook;
            this.NameAutor = NameAutor;
            this.Year = Year;
            this.CountDays = CountDays;
        }
        //for BD books after close program
        public Book(string NameBook, string NameAutor, int Year, int CountDays, string Status, DateTime TimeToRead, DateTime DaysLeft)
        {
            this.NameBook = NameBook;
            this.NameAutor = NameAutor;
            this.Year = Year;
            this.CountDays = CountDays;
            this.Status = Status;
            this.TimeToRead = TimeToRead;
            this.DaysLeft = DaysLeft;
        }
    }
}