﻿using System;

namespace HW6._1
{
   public class StartGame
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Let's start");
            Transformers transformers = new Transformers(); // создали объект
            MainMenu(transformers); // вызвали наш метод главного меню


        }
       
        public static void MainMenu(Transformers transformers) // принмаю значение типа моего экземпляра
        {
          
            Console.WriteLine("If u want to choose Transformer press 1\n" +
                " If u want to choose Humanoid press 2\n");
            var role = Console.ReadLine();
            if (role.Equals("1"))   // тут я сравниваю
            {
                transformers.State = "Transformer";
                Console.WriteLine("Now I'm Transformer");
                var res1 = ChooseTransformer(transformers); // передаю значение
                Console.WriteLine(res1);
                var scan = transformers.Scanner;
                Console.WriteLine(scan);
                Console.WriteLine("\nPress Enter");
                Console.ReadKey();
                Console.Clear();
                ReStart(transformers);
             
            }
            else if (role.Equals("2"))
            {
                Console.WriteLine("Now I'm Humanoid.Xo-Xo");
                transformers.State = "Humanoid"; // присваиваю значение гуманоида
                var res1 = HumanoidActions(transformers); //сохраняю свой результат
                Console.WriteLine(res1);
                Console.WriteLine("\nPress Enter");
                Console.ReadKey();
                Console.Clear();
                ReStart(transformers);
            }
        }
        public static string ReStart(Transformers transformers)
        {
           
            Console.WriteLine("If you Want to choose again press 1\n" +
                    "If you Want to exit press 2\n");
            var res2 = Console.ReadLine();
            switch (res2)
            {
                case "1":
                    MainMenu(transformers);
                    break;
                case "2":    // или можно через default
                    Console.WriteLine("Bye bye guys");
                    break;
            }
            return res2;
        }
        public static string ChooseTransformer(Transformers transformers)
        {
            
            string res = "";
            Console.WriteLine("Choose Transformer:\n" +
                "SwimTrasformer --> 1\n" +
                "RideTrasformer --> 2\n" +
                "FlyTrasformer --> 3\n");
            var result2 = Console.ReadLine();
            switch (result2)
            {
                case "1":
                    res = transformers.SwimTransforming();
                    break;
                case "2":
                    res = transformers.RideTransforming();
                    break;
                case "3":
                    res = transformers.FlyTransforming();
                    break;
            }
            return res;

        }
        public static string HumanoidActions(Transformers transformers)
        {
           
            string Result = "";
            Console.WriteLine("Choose action :\n" +
                "1 --> run\n" +
                "2 --> shooting\n");
            var result3 = Console.ReadLine();
            switch (result3)
            {
                case "1":
                    Result = transformers.Run();
                    break;

                case "2":
                    Result = transformers.Shoot();
                    HumanoidWeapons();           //подумать где вызвать оружие
                    break;
            }
            return Result;
        }
        public static string HumanoidWeapons()
        {
         
            string result = "";
            Console.WriteLine("Well choose weapon\n" +
                "1 -> CosmoPistoleto\n" +
                "2 -> CosmoBazuka\n" +
                "3 -> CosmoRaketo\n" +
                "4 -> CosmoLazero\n");
            var weapons = Console.ReadLine();
            switch (weapons)
            {
                case "1":
                    result = "CosmoPistoleto"; // тут я присваиваю значение
                    break;
                case "2":
                    result = "CosmoBazuka";
                    break;
                case "3":
                    result = "CosmoRaketo";
                    break;
                case "4":
                    result = "CosmoLazero";
                    break;
            }
            return result;
        }

    }
}


