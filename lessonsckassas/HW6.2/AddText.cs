﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection.Metadata;

namespace HW6._2
{
    internal interface IFileStream
    {
        void SaveTextCsv(string text);
        void SaveTextPdf(string text);
        void SaveTextTxt(string text);
    }

    public class AddText : IFileStream
    {
        public static string KatalogTxt = @"D:\tanya.txt"; // путь в наш файл
        public static string KatalogPdf = @"D:\tanya.pdf";
        public static string KatalogCsv = @"D:\tanya.csv";


        public void SaveTextCsv(string text)
        {
            try
            { //Encoding содержит кодировку выходных данных
                using (StreamWriter res = new StreamWriter(KatalogCsv, false, System.Text.Encoding.Default)) // создаем объект
                {
                    res.WriteLine(text);  // сама запись в файл
                }

                //using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                //{
                //    sw.WriteLine("Дозапись");
                //    sw.Write(4.5);
                //}
                Console.WriteLine("Запись выполнена");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void SaveTextPdf(string text)
        {


            Console.WriteLine("Method is in revision");
            //using (var stream = File.Create(Path.Combine(KatalogPdf, "tanya.pdf")))
            //{
            //    var formFieldMap = PDFHelper.GetFormFieldNames(KatalogPdf);
            //    var pdfContents = PDFHelper.GeneratePDF(KatalogPdf, formFieldMap);

            //    stream.Write(pdfContents, 0, pdfContents.Length);

            //}

            //var doc = new Document();

            //PdfWriter.GetInstance(doc, new FileStream(ApplicationException.StartupPath + KatalogPdf, FileMode.Create));
            //doc.Open();

        }

        public void SaveTextTxt(string text)  // в скобках параметр который мы принимаем
        {
            try
            { //Encoding содержит кодировку выходных данных
                using (StreamWriter res = new StreamWriter(KatalogTxt, false, System.Text.Encoding.Default)) // создаем объект
                {
                    res.WriteLine(text);  // сама запись в файл
                }

                //using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                //{
                //    sw.WriteLine("Дозапись");
                //    sw.Write(4.5);
                //}
                Console.WriteLine("Запись выполнена");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
