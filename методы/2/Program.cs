﻿using System;


namespace _2
{
    using System;

    namespace number2
    {
        class Program
        {
            static void Main(string[] args)
            {
                // Вводим число (0-999), получаем строку с прописью числа.

                string[] number1 = { "", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот" };
                string[] number2 = { "", "", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто" };
                string[] number3 = { "", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять",
                          "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семьнадцать", "восемьнадцать", "девятнадцать" };

                string PrintNumber(int n) => string.Format("{0} {1} {2}", number1[n / 100 % 10], number2[n / 10 % 10], n == 0 ? "ноль" : n % 100 < 20 ? number3[n % 100] : number3[n % 10]).Trim().Replace("  ", " ");


                Console.WriteLine(PrintNumber(int.Parse(Console.ReadLine())));
                Console.ReadKey();


            }
        }
    }
}
       
    
