﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Разработать методы вывода фигур из ноликов
            //    (квадрат, треугольник прямоугольный, треугольник равносторонний, 
            //    еревернутые треугольники, песочные часы).

            void PrintSquare()
            {
                for (int i = 0; i < 4; i++)
                {
                    Console.WriteLine("0 0 0 0");
                }
            }

            void PrintRectTriangle()
            {
                for (int i = 0; i < 6; i++)
                {
                    for (int j = 6 - i; j < 6; j++)
                    {
                        Console.Write("0 ");
                    }
                    Console.WriteLine();
                }
            }

            void PrintRevRectTriangle()
            {
                for (int i = 0; i < 6; i++)
                {
                    for (int j = i; j < 6; j++)
                    {
                        Console.Write("0 ");
                    }
                    Console.WriteLine();
                }
            }

            void PrintEquilateralTriangle()
            {
                string[] rows = { "  0  ", " 000 ", "00000" };
                for (int i = 0; i < rows.Length; i++)
                {
                    Console.WriteLine(rows[i]);
                }
            }

            void PrintRevEquilateralTriangle()
            {
                string[] rows = { "  0  ", " 000 ", "00000" };
                for (int i = rows.Length - 1; i >= 0; i--)
                {
                    Console.WriteLine(rows[i]);
                }
            }

            void PrintTimer()
            {
                PrintRevEquilateralTriangle();
                PrintEquilateralTriangle();
            }

            Console.WriteLine("Выберите фигуру: " +
                "квадрат - 1, треугольник прямоугольный - 2, треугольник равносторонний - 3, " +
                "перевернутый треугольник прямоугольный - 4, перевернутый треугольник равносторонний - 5, " +
                "песочные часы - 6");
            string input;
            while ((input = Console.ReadLine()) != "stop")
            {
                int choice = int.Parse(input);
                switch (choice)
                {
                    case 1:
                        PrintSquare();
                        break;
                    case 2:
                        PrintRectTriangle();
                        break;
                    case 3:
                        PrintEquilateralTriangle();
                        break;
                    case 4:
                        PrintRevRectTriangle();
                        break;
                    case 5:
                        PrintRevEquilateralTriangle();
                        break;
                    case 6:
                        PrintTimer();
                        break;
                    default:
                        Console.WriteLine("Ошибка выбора");
                        break;
                }

            }
        }
    }
}