﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3
{
   public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Выберите,что посчитать : периметр - 1, Радиус окружности - 2, Площадь - 3");
            char choise = char.Parse(Console.ReadLine());
            float radius; //float занимает меньше памяти
            float square;
            float diametr = 0;

            switch (choise)
            {
                case '1':
                    Console.WriteLine("Введите радиус круга: ");
                    radius = float.Parse(Console.ReadLine());
                    Console.WriteLine("Периметр равен " + Perimetr(radius) + " " + diametr); //out diametr запоминает параметр,+ diametr чтобы и диаметр выводило
                    break;

                case '2':
                    Console.WriteLine("Введите площадь круга: ");
                    square = float.Parse(Console.ReadLine());
                    Console.WriteLine("Радиус круга равен " + Radius(square));
                    break;

                case '3':
                    Console.WriteLine("Введите радиус круга: ");
                    radius = float.Parse(Console.ReadLine());
                    Console.WriteLine("Площадь круга равна " + Square(radius));
                    break;
            }

        }

       public static double Perimetr(double r)
        {
           
            double perimetr = r * 2 * 3.14;
            return perimetr;
        }
       public static double Radius(double d)
        {
            double radius = d/2;
            return radius;
        }
      public  static double Square(double s)
        {
            double radius = Math.Sqrt(s * 3.14);
            return radius;
        }
    }
}
