﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _3
{
    class Program
    {
        static void Main(string[] args)
        {
            { //Вводим строку, которая содержит число, написанное прописью(0 - 999).Получить само число*/
                var input = Console.ReadLine();
                var split = input.Split();

                int FindSimpleNumber(string str)
                {
                    int res = 0;
                    string[] firstTen = { "ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять" };
                    string[] _11_19 = { "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семьнадцать", "восемьнадцать", "девятнадцать" };
                    string[] hundreds = { "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот" };
                    if (str.EndsWith("надцать"))
                    {
                        for (int i = 0; i < _11_19.Length; i++)
                        {
                            if (str == _11_19[i])
                            {
                                res = 11 + i;
                                break;
                            }
                        }
                        return res;
                    }

                    if (str.EndsWith("дцать"))
                    {
                        if (str == "двадцать")
                            return 20;
                        return 30;
                    }

                    if (str.EndsWith("десят"))
                    {
                        switch (str)
                        {
                            case "пятьдесят":
                                res = 50;
                                break;
                            case "шестьдесят":
                                res = 60;
                                break;
                            case "семьдесят":
                                res = 70;
                                break;
                            case "восемьдесят":
                                res = 80;
                                break;
                        }
                        return res;
                    }

                    if (str == "сорок")
                    {
                        res = 40;
                    }
                    else if (str == "девяносто")
                    {
                        res = 90;
                    }
                    else
                    {
                        for (int i = 0; i < firstTen.Length; i++)
                        {
                            if (str == firstTen[i])
                            {
                                res = i;
                                break;
                            }

                        }
                        for (int j = 0; j < hundreds.Length; j++)
                        {
                            if (str == hundreds[j])
                            {
                                res = 100 * (j + 1);
                                break;
                            }
                        }
                    }

                    return res;
                }

                var num = 0;
                foreach (var s in split)
                {
                    num += FindSimpleNumber(s);
                }

                Console.WriteLine($"{num}");
            }
        }
    }
}


