using NUnit.Framework;
using HW3;

namespace HW3NUnit.Test
{
    public class Tests
    {


        [TestCase(1, 6.28)] // (double r,expected)
        [TestCase(-1, -6.28)]
        [TestCase(0, 0)]
        public void Test1(double r, double expected)
        {
            Program program = new Program();
            var actual = Program.Perimetr(r);
           
            Assert.AreEqual(expected,actual);
        }

        [TestCase(2, 1)] // (double r,expected)
        [TestCase(-2, -1)]
        [TestCase(0, 0)]
        public void Test3(double d, double expected)
        {
            Program program = new Program();
            var actual = Program.Radius(d);
            Assert.AreEqual(expected, actual);
        }
       
        [TestCase(2, 2.5059928172283334)] // (double r,expected)
        [TestCase(5, 3.96232255123179)]
        [TestCase(0, 0)]
        public void Test4(double s, double expected)
        {
            Program program = new Program();
            var actual = Program.Square(s);
            Assert.AreEqual(expected, actual);
        }

    }
}