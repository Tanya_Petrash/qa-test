﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Пожалуйста, задайте размер книги:");
            int bookSize = int.Parse(Console.ReadLine());
            string[] book = new string[bookSize];
           Console.WriteLine("Введите данные действие, имя ");
            string input;
            while ((input = Console.ReadLine()) != "stop") //для завершения работы
            {
                Eval(input, book);
            }
            Console.WriteLine("Завершаем работу");
        }

        static void Eval(string input, string[] book) 
        {
            var split = input.Split(' '); //сплитим,чтобы заменять элементы массива действиями
            string word = split[0]; 
            switch (word)
            {
                case "add":
                    Add(book, split[1]); //наша книга и в ней над 2м элементом массива делаем действия
                    break;
                case "delete":
                    Delete(book, split[1]);//наша книга и в ней над 2м элементом массива делаем действия
                    break;
                case "list":
                    ListBook(book);//выводим книгу
                    break;
                default:
                    Console.WriteLine($"Неизвестная опреация {word}");
                    break;
            }
        }

        static void ListBook(string[] book)
        {
            foreach (string name in book)
            {
                if (name != null)  //выводим все данные в книге,кроме значения null
                {
                    Console.WriteLine(name);
                }                
            }
        }

        static void Delete(string[] book, string name)
        {
            int i = 0; // i имя в массиве под 0 индексом
            while (i < book.Length) // перебираем весь массив
            {
                if (book[i] == name)
                {
                    book[i] = null;
                    break;
                }
                i++;
            }
            if (i == book.Length)
            {
                Console.WriteLine($"Имя {name} не найдено в книге");
                return;
            }

            for (int j = i + 1; j < book.Length; j++) //i + 1 этим указываем на то,что хотим справа начать действия
            {
                book[j - 1] = book[j]; //[j - 1] т к индекс нумеруются с 0, происходит сдвиг
            }
            book[book.Length - 1] = null; // берем в учет,что после удаления имя сдвигается на индекс ранее и в старом индексе тоже остается,чтобы не дублировалось делаем это действие
        }

        static void Add(string[] book, string name)
        {
            int i = 0;
            while (i < book.Length)
            {
                if (book[i] == null)
                {
                    break;
                }
                i++;
            }
            if (i == book.Length)
            {
                Console.WriteLine("Объем книги исчерпан");
            }
            else
            {
                book[i] = name; // имя добавляем с конца
            }
        }

        }
    }

