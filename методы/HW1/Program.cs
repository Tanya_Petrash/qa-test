﻿using System;

namespace Homework4
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculator_HW4();
        }
        static public void SetData(out int num2, out char sign)
        {
            Console.WriteLine("Введите 2 число");
            num2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите действие в виде знака + - / * ");
            sign = char.Parse(Console.ReadLine());
            if ((num2 == 0 && sign == '/') || (sign != '/' && sign != '*' && sign != '+' && sign != '-'))
            {
                Console.WriteLine("Error");
                SetData(out num2, out sign); // метод вызывает сам себя
            }

        }
        static public int Calculate(int num1, int num2, char sign)
        {
            int result = 0;
            switch (sign)
            {
                case '+':
                    result = num1 + num2;
                    break;
                case '-':
                    result = num1 - num2;
                    break;
                case '/':
                    result = num1 / num2;
                    break;
                case '*':
                    result = num1 * num2;
                    break;
            }
            return result;
        }
        public enum Answer  // ответ пользователя
        {
            New,
            Continue,
            End
        }
        static public Answer IsCalculateFinish()
        {
            Console.WriteLine("Введите New если хотите начать новые вычесления, Continue если хотите продолжить вычесления и End если хотите завершить");
            Answer answer = (Answer)Enum.Parse(typeof(Answer), Console.ReadLine());
            return answer;
        }
        static public int SetNum1()
        {
            Console.WriteLine("Введите 1 число");
            int num = int.Parse(Console.ReadLine()); //первое число
            return num;
        }
        static public void Calculator_HW4()
        {
            int num1;
            char sign;
            Answer answer = Answer.New;
            while (answer == Answer.New) // пока польтзователь не введет другой ответ
            {
                num1 = SetNum1();
                answer = Answer.Continue;
                while (answer == Answer.Continue) //операции со старым результатом
                {
                    SetData(out int num2, out sign);
                    num1 = Calculate(num1, num2, sign);
                    Console.WriteLine(num1);
                    answer = IsCalculateFinish();//завершить действия со старым результатом
                }
            }
        }
    }

}


    