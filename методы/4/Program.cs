﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4
{
    class Program
    {
        static void Main(string[] args)
        {
            // Найти расстояние между двумя точками в двумерном декартовом пространстве.
            Console.WriteLine("enter x");
            double x = int.Parse(Console.ReadLine());
            Console.WriteLine("enter y");
            double y = int.Parse(Console.ReadLine());
            Console.WriteLine("enter x1");
            double x1 = int.Parse(Console.ReadLine());
            Console.WriteLine("enter y1");
            double y1 = int.Parse(Console.ReadLine());
            Console.WriteLine(Distance(x,y,x1,y1));


        }

        public static double Distance(double x, double y, double x1, double y1)
        {
            double result = 0;
            if (x == x1)
                result = y1 - y;
            else if (y == y1)
                result = x1 - x;
            else if (x != x1 && y != y1)
                result = Math.Sqrt(Math.Pow(x1 - x, 2) + Math.Pow(y1 - y, 2)); // Math.Sqrt извлекаем кв корень из числа, Math.Pow делаем кв корень

            return result;

        }



    }
}
