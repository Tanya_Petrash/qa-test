using System;
using Xunit;
using HW3;

namespace HW3xUnit.Test
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Program program = new Program();
            var expected = 25.12;
            var actual = Program.Perimetr(4);
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData(1, 6.28)] // (double r,expected)
        [InlineData(-1, -6.28)]
        [InlineData(0, 0)]
        public void Test2(double r, double expected)
        {
            Program program = new Program();
            var actual = Program.Perimetr(r);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(2, 1)] // (double r,expected)
        [InlineData(-2, -1)]
        [InlineData(0, 0)]
        public void Test3(double d, double expected)
        {
            Program program = new Program();
            var actual = Program.Radius(d);
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData(2, 2.5059928172283334)] // (double r,expected)
        [InlineData(5, 3.96232255123179)]
        [InlineData(0, 0)]
        public void Test4(double s, double expected)
        {
            Program program = new Program();
            var actual = Program.Square(s);
            Assert.Equal(expected, actual);
        }

    }
}
